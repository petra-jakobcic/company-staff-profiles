// Get the HTML elements that we need.
const form = document.getElementById("form");
const nameInput = document.getElementById("full-name");
const titleInput = document.getElementById("title");
const bioInput = document.getElementById("bio");
const button = document.getElementById("form-button");
const profileSection = document.getElementById("profile-section");

// Write the functions that we need.
/**
 * Creates an object that contains the current
 * values of the input boxes.
 * 
 * @returns {Object} The input values.
 */
function getInputValues() {
    const fullName = nameInput.value;
    const title = titleInput.value;
    const bio = bioInput.value;

    const myObject = {
        fullName,
        title,
        bio: bio
    };

    return myObject;
}

/**
 * Creates a new profile.
 * 
 * @param {string} imageSource The profile image.
 * @param {string} fullName The staff member's name.
 * @param {string} title The job title.
 * @param {string} bio A short biography.
 * 
 * @returns {HTMLDivElement} The new profile.
 */
function createProfile(imageSource, fullName, title, bio) {
    // Create the elements we need.
    const profileDiv = document.createElement("div");
    const imageImg = document.createElement("img");
    const fullNameH4 = document.createElement("h4");
    const titleH6 = document.createElement("h6");
    const bioP = document.createElement("p");

    // Set their inner HTML.
    imageImg.src = imageSource;
    fullNameH4.innerHTML = fullName;
    titleH6.innerHTML = title;
    bioP.innerHTML = bio;

    // Add a class to each new element.
    imageImg.classList.add("profile__image");
    profileDiv.classList.add("profile");
    fullNameH4.classList.add("profile__name");
    titleH6.classList.add("profile__title");
    bioP.classList.add("profile__bio");

    // Build the profile.
    profileDiv.appendChild(imageImg);
    profileDiv.appendChild(fullNameH4);
    profileDiv.appendChild(titleH6);
    profileDiv.appendChild(bioP);

    return profileDiv;
}

/**
 * Responds to the button click.
 *
 * @param {Event} event An event object.
 *
 * @returns void
 */
 function formSubmitListener(event) {
    event.preventDefault(); // prevents the default behaviour of the form to be submitted

    const values = getInputValues();

    const profile = createProfile(
        "img/user.svg",
        values.fullName,
        values.title,
        values.bio
    );

    profileSection.appendChild(profile);

    form.reset();
}

// Tie it all together.
form.addEventListener("submit", formSubmitListener);
